export class Comision {
  id: number;
  facultad: string;
  carrera: string;
  materiaCodigo: number;
  materiaNombre: string;
  catedra: string;
  turno: string;
  semestre: number;
  anio: number;
  cantidadAlumnos: number;
}
