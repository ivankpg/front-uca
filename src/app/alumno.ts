export class Alumno {
  alumnoId: number;
  legajo: number;
  carreraNombre: string;
  carreraCodigo: number;
  carreraId: number;
  regular: boolean;
  nota?: string;
  nombreAlumno: string;
  tieneNota: string;
}
