import {Component, Input, OnInit} from '@angular/core';
import {Filtro} from '../filtro';
import {Comision} from '../comision';
import { ComisionService } from '../services/comision.service';
import {Acta} from '../acta';

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.css']
})
export class FiltrosComponent implements OnInit {
  // anio = 'anio';
  constructor(private comisionService: ComisionService) { }

  comisiones : Comision[];

  ngOnInit() {
  }

  buscar(anio: string, turno: string, materia: string, catedra: string, semestre: string, carrera: string): void {

    this.comisionService
      .getComisionesFiltro(anio, turno, materia, catedra, semestre, carrera)
      .subscribe(comisiones => this.comisiones = comisiones);
  }

}
