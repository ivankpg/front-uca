import { Component, OnInit, ViewChild, Input, HostListener } from '@angular/core';
import { Comision } from '../comision';
import { Filtro } from '../filtro';
import { DataTableDirective } from 'angular-datatables';
import { ComisionService } from '../services/comision.service';


@Component({
  selector: 'app-comisiones',
  templateUrl: './comisiones.component.html',
  styleUrls: ['./comisiones.component.css']
})
export class ComisionesComponent implements OnInit {
  constructor(private comisionService: ComisionService) {
  }

  @Input('comisiones') comisiones: Comision[];

  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};



  ngOnInit() {

    //this.getComisiones();

  }

   // getComisiones(): void {
   //   this.dtOptions = {
   //     pagingType: 'simple_numbers',
   //     pageLength: 500,
   //     serverSide: true,
   //     ordering: false,
   //     processing: true,
   //     searching: false,
   //     responsive: true,
   //     info: false,
   //     paging: false,
   //     ajax: (dataTablesParameters: any, callback) => {
   //       this.comisionService
   //         .postComisionesDatatable(dataTablesParameters)
   //         .subscribe(resp => {
   //           this.comisiones = resp.data;
   //
   //           callback({
   //             recordsTotal: resp.recordsTotal,
   //             recordsFiltered: resp.recordsFiltered,
   //             data: []
   //           });
   //         });
   //     },
   //     columns: [
   //       {data: 'id'},
   //       {data: 'materiaNombre'},
   //       {data: 'catedra'},
   //       {data: 'turno'},
   //       {data: 'semestre'},
   //       {data: 'anio'}
   //       ]
   //   };
   // }

  // getComisiones(): void {
  //   this.comisionService
  //     .getComisionesFiltro(this.anio)
  //     .subscribe(comisiones => this.comisiones = comisiones);
  // }

}
