import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ComisionesComponent } from './comisiones/comisiones.component';
import { FiltrosComponent } from './filtros/filtros.component';

import { FormsModule } from '@angular/forms';
import { ComisionDetailComponent } from './comision-detail/comision-detail.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';

import { DataTablesModule } from 'angular-datatables';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ComisionVerComponent } from './comision-ver/comision-ver.component';

@NgModule({
  declarations: [
    AppComponent,
    ComisionesComponent,
    ComisionDetailComponent,
    FiltrosComponent,
    ComisionVerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    DataTablesModule,
    NgbModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
