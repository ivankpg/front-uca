import { Component, OnInit } from '@angular/core';
import {Alumno} from '../alumno';
import {ActivatedRoute} from '@angular/router';
import {ComisionService} from '../services/comision.service';
import {Location} from '@angular/common';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-comision-ver',
  templateUrl: './comision-ver.component.html',
  styleUrls: ['./comision-ver.component.css']
})
export class ComisionVerComponent implements OnInit {

  alumnos: Alumno[];

  constructor(
    private route: ActivatedRoute,
    private comisionService: ComisionService,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getAlumnosComisionVer();
  }

  getAlumnosComisionVer(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.comisionService
      .getAlumnosComisionVer(id)
      .subscribe(alumnos => this.alumnos = alumnos);
  }

  goBack(): void {
    this.location.back();
  }

}
