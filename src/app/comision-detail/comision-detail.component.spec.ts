import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComisionDetailComponent } from './comision-detail.component';

describe('ComisionDetailComponent', () => {
  let component: ComisionDetailComponent;
  let fixture: ComponentFixture<ComisionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComisionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComisionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
