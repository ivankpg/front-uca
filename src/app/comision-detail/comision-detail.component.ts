import { Component, OnInit, Input} from '@angular/core';
import { Comision } from '../comision';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ComisionService } from '../services/comision.service';
import { Alumno } from '../alumno';
import { Acta } from '../acta';
import { HttpClient, HttpResponse } from '@angular/common/http';

@Component({
  selector: 'app-comision-detail',
  templateUrl: './comision-detail.component.html',
  styleUrls: ['./comision-detail.component.css']
})
export class ComisionDetailComponent implements OnInit {
  alumnos: Alumno[];
  private actas: Acta[] = [];

  dtOptions: DataTables.Settings = {};

  constructor(
    private route: ActivatedRoute,
    private comisionService: ComisionService,
    private location: Location,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getAlumnosComision();
  }

  getAlumnosComision(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.comisionService
      .getAlumnosComision(id)
      .subscribe(alumnos => this.alumnos = alumnos);
  }

  goBack(): void {
    this.location.back();
  }

  onSubmit(): void {

    const idComision = +this.route.snapshot.paramMap.get('id');

    this.alumnos.forEach(a=>{
      if (a.nota !== "" && !a.tieneNota) {
        const ac: Acta = {
          idAlumno: a.alumnoId,
          idComision: idComision,
          nota: +a.nota
        };
        this.actas.push(ac);
      }

    });

    this.comisionService.cargarNotas(this.actas)
      .subscribe(() => this.goBack());
  }


}
