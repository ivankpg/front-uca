import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComisionDetailComponent }  from './comision-detail/comision-detail.component';
import { FiltrosComponent }  from './filtros/filtros.component';
import {ComisionVerComponent} from './comision-ver/comision-ver.component';

const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
  { path: 'comisiones', component: FiltrosComponent },
  { path: 'comisiones/:id/alumnos', component: ComisionDetailComponent },
  { path: 'comisiones/:id/ver', component: ComisionVerComponent }
  ];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}


