import { Injectable } from '@angular/core';
import { Comision } from '../comision';
import { Alumno } from '../alumno';
import { Observable, of } from 'rxjs';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
// Import RxJs required methods
import { map } from 'rxjs/operators';
import { DataTablesResponse } from '../datatableResponse';
import {Acta} from '../acta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ComisionService {


  private apiUrl = 'http://api.uca.local';  // URL to web api
  private withApiPath = `${this.apiUrl}/api`;  // URL to web api
  private comisionesUrl = `${this.withApiPath}/comisiones`;

  constructor(
    private http: HttpClient) { }


  getAlumnosComision (id: number): Observable<Alumno[]> {
    const url = `${this.comisionesUrl}/${id}/alumnos`;
    return this.http.get<HttpResponse<Alumno[]>>(url, {observe: 'response'})
    .pipe(map(response => response.body['data']));
  }

  getAlumnosComisionVer (id: number): Observable<Alumno[]> {
    const url = `${this.comisionesUrl}/${id}`;
    return this.http.get<HttpResponse<Alumno[]>>(url, {observe: 'response'})
    .pipe(map(response => response.body['data']));
  }

  cargarNotas (actas: Acta[]): Observable<Acta[]> {
    const url = `${this.withApiPath}/actas`;
    return this.http.post<Acta[]>(url, actas, httpOptions);
  }

  getComisionesFiltro(anio: string, turno: string, materia: string, catedra: string, semestre: string, carrera: string)
    : Observable<Comision[]> {
    const parametros = `?anio=${anio}&turno=${turno}&materia=${materia}&catedra=${catedra}&semestre=${semestre}&carrera=${carrera}`;
    return this.http.get<HttpResponse<Comision[]>>(`${this.comisionesUrl}${parametros}`,{observe: 'response'})
      .pipe(map(response => response.body['data']));
  }

}
